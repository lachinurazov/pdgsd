#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>
#include "Header.h"
#include "Message.h"

using namespace std;

/*
class CSVRow
{
public:
    string const& operator[](std::size_t index) const
    {
        return m_data[index];
    }
    size_t size() const
    {
        return m_data.size();
    }
    void readNextRow(istream& str)
    {
        string line;
        getline(str, line);

        stringstream lineStream(line);
        string cell;

        m_data.clear();
        while (std::getline(lineStream, cell, ',')) {
            m_data.push_back(cell);
        }
        // This checks for a trailing comma with no data after it.
        if (!lineStream && cell.empty()) {
            // If there was a trailing comma then add an empty element.
            m_data.push_back("");
        }
    }

private:
    vector<string> m_data;
};
*/
std::istream& operator>>(ifstream& str, CSVRow& data)
{
    data.readNextRow(str);
    return str;
}

void openFileForRead(ifstream& configFile)
{
    string input = "";
    bool fileIsOpened = false;
    while (fileIsOpened != true) {
        cout << "Please enter the location of the configuration file:\n";
        cout << "The configuration file must be a CSV ";
        getline(cin, input);
        configFile.open(input.c_str());

        if (!configFile.is_open()) {
            cout << "Unable to open the config file. Please try again\n";
        } else {
            fileIsOpened = true;
        }
    }
}
void openFileForWrite(ofstream& resultFile)
{
    string input = "";
    bool fileIsOpened = false;
    while (fileIsOpened != true) {
        cout << "Please enter the location of the result file:\n";
        cout << "The result file will be a CSV ";
        getline(cin, input);
        resultFile.open(input.c_str());

        if (!resultFile.is_open()) {
            cout << "Unable to open the result file. Please try again\n";
        } else {
            fileIsOpened = true;
        }
    }
}


int main(int argc, char** argv)
{
    ifstream configFile;
    openFileForRead(configFile);
    ofstream resultFile;
    openFileForWrite(resultFile);
    string messageString = "";
    cout << "Please enter the SIP Message that needs to be parsed\n";
    getline(cin, messageString);
    CSVRow row;
    vector<Header> headers;
    while (configFile >> row) {
        Header header(row);
        headers.push_back(header);
    }

    Message msg(messageString);
    std::map<string, string> foundHeaders;
    msg.find_headers(headers, resultFile);

    return 0;
}

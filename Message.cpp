
#include "Header.h"
#include "Message.h"

class Header;
using namespace std;

const void split(const string& str, std::vector<string>& result)
{
    stringstream ss(str);
    string tok;
    while (getline(ss, tok)) {
        result.push_back(tok);
    }
}

const void ltrim(std::string& s)
{
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), std::not1(std::ptr_fun<int, int>(std::isspace))));
}

const void rtrim(std::string& s)
{
    s.erase(std::find_if(s.rbegin(), s.rend(), std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}

// trim from both ends (in place)
static void trim(std::string& s)
{
    ltrim(s);
    rtrim(s);
}

Message::Message(const string& messageString) : m_messageStr(messageString)
{
    split(m_messageStr, m_rows);
    // ignore the first line because it's a request line
    for (unsigned int i = 1; i < m_rows.size(); i++) {
        size_t separator = (m_rows[i]).find_first_of(':');
        string headerName = m_rows[i].substr(0, separator);
        trim(headerName);
        cout << "Header Name = " << headerName << endl;
        if (separator != string::npos) {
            string headerValue = m_rows[i].substr(separator + 1, m_rows[i].size());
            trim(headerValue);

            // peek next row to make sure the value does not span multiple lines
            unsigned int peekLineNumber = i + 1;
            bool peekNextLine = true;
            while (peekNextLine) {
                if (peekLineNumber < m_rows.size() && m_rows[peekLineNumber].find(' ') == 0) {
                    headerValue.append(" ");
                    string nextLine = m_rows[peekLineNumber];
                    ltrim(nextLine);
                    headerValue.append(nextLine);
                    peekLineNumber++;
                } else {
                    peekNextLine = false;
                    i = peekLineNumber - 1;
                }
            }
            cout << "Header Value = " << headerValue << endl;
            m_headers[headerName] = headerValue;
        }
    }
}

void Message::find_headers(const vector<Header>& headers, ofstream& resultFile)
{
    for (size_t i = 0; i < headers.size(); i++) {
        resultFile << "-------------------------" << endl;
        // It's too late and i can't figure out why i can't use header_name() without copying the objects
        Header header = headers[i];
        const string& headerName = header.header_name();
        map<string, string>::iterator search = m_headers.find(header.header_name());
        if (search != m_headers.end()) {
            resultFile << search->first << ":" << search->second << endl;
            if (header.parse_uri_parameters()) {
                // assuming we got a uri
                string uri = search->second;
                size_t index = uri.find_first_of("@");
                string userPortion = uri.substr(0, index);
                string serverPortion = uri.substr(index + 1, uri.size());
                cout << "UserPortion = " << userPortion << endl;
                cout << "serverPortion = " << serverPortion << endl;
                vector<string> uriParams = header.uri_parameters();
                for (size_t i = 0; i < uriParams.size(); i++) {
                    size_t position = userPortion.find(uriParams[i]);
                    if (position != string::npos) {
                        cout << "String = " << userPortion.substr(position, userPortion.size());
                    } else {
                        position = serverPortion.find(uriParams[i]);
                        if (position != string::npos) {
                            string parameterPlusRest =
                                serverPortion.substr(position + uriParams[i].size() + 1, serverPortion.size());
                            parameterPlusRest = parameterPlusRest.substr(0, parameterPlusRest.find(">"));
                            string parameter = parameterPlusRest.substr(0, parameterPlusRest.find(";"));
                            resultFile << uriParams[i] << "=" << parameter << endl;
                        }
                    }
                }
            }
        }

        // cout << m_headers[(];
    }
}
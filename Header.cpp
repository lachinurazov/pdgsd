#include <string>
#include <vector>
#include "Header.h"

Header::Header() : m_parseUriParameters(false)
{
}

Header::Header(const CSVRow& row) : m_parseUriParameters(false)
{
    if (row.size() < 2) {
        cout << "Improperly formatted row. Skip parsing\n";
        return;
    }
    m_headerName = row[0];
    string parseUriParametersString = row[1]; 
    transform(parseUriParametersString.begin(), parseUriParametersString.end(), parseUriParametersString.begin(), ::tolower);
    m_parseUriParameters = parseUriParametersString == "yes" || row[1] == "y" ? true : false;
    if (m_parseUriParameters) {
        for (unsigned int i = 2; i < row.size(); i++) {
            m_uriParameters.push_back(row[i]);
        }
    }
    cout << "Header name: " << m_headerName << endl;
}
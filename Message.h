#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <algorithm>
#include <map>

using namespace std;

class Header;
class Message
{

public:
    Message() = delete;
    Message(const string& messageString);
    void find_headers(const vector<Header>& headers, ofstream& resultFile);

private:
    string m_messageStr;
    vector<string> m_rows;
    map<string, string> m_headers;
};
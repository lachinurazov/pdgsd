#include "CSVRow.h"

class Header
{
public:
    Header(const CSVRow& row);
    inline const string& header_name()
    {
        return m_headerName;
    };
    inline bool parse_uri_parameters()
    {
        return m_parseUriParameters;
    };
    inline const vector<string>& uri_parameters()
    {
        return m_uriParameters;
    };
private:
    Header();
    string m_headerName;
    bool m_parseUriParameters;
    vector<string> m_uriParameters;
};